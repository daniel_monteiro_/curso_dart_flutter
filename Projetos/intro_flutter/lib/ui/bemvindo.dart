import 'package:flutter/material.dart';

class BemVindoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: Center(
        child: Text("Hello, universe!", textDirection: TextDirection.ltr,
          style: TextStyle(fontSize: 34.0, fontWeight: FontWeight.w700),),
      ),

    );
  }
}