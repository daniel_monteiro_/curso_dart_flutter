import 'package:flutter/material.dart';

class Coluna extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.blueAccent,
      child: Stack(
        alignment: Alignment.center,
//        mainAxisAlignment: MainAxisAlignment.spaceEvenly, // o que está dentro da coluna fica no centro
        children: <Widget>[
          Text("Primeiro", textDirection: TextDirection.ltr,
            style: TextStyle(fontStyle: FontStyle.italic, fontSize: 16.0),
          ),
//          Expanded(child: Text("Outro")), // ocupa todo espaço disponível
//          Flexible(child: Text("Outro")), // Se encaixa em seu espaço nencessário
//          Flexible(child: Text("Ainda")),

          Padding(padding: EdgeInsets.all(13.0), // Colocar padding para seus widgets filhos
            child: FlatButton(onPressed: () => {}, child: Text("Botão")),
          ),
          
          Text("Segundo", textDirection: TextDirection.ltr,
            style: TextStyle(fontStyle: FontStyle.normal, fontSize: 16.0),
          ),

          FlatButton(
            onPressed: () => "Olá!",
            child: Text("Botão"),
            color: Colors.green,
          )
        ],
      ),
    );
  }

}