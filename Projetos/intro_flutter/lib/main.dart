import 'package:flutter/material.dart';
import 'package:intro_flutter/ui/coluna.dart';

import 'ui/bemvindo.dart';

void main() {
  runApp(
    MaterialApp(
      title: "Olá!",
      color: Colors.blueAccent,
      home: Coluna(),
    ),
  );
}
