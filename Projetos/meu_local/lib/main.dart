import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  List locais = [
    "Praias: Fortaleza; Recife",
    "Olinda - PE",
    "Brasília",
    "Cuiabá",
    "China"
  ];

  List<MaterialColor> cores = [
    Colors.blue,
    Colors.orange,
    Colors.green,
    Colors.pink,
    Colors.red,
    Colors.grey
  ];

  String randomTexto = "";
  MaterialColor randomCore = Colors.blue;

  void _atualizarTela() {
    setState(() {
      randomTexto = this.locais[Random().nextInt(this.locais.length-1)];
      randomCore = this.cores[Random().nextInt(this.cores.length-1)];
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            FlatButton(
              onPressed: () {
                this._atualizarTela();
              },
              color: randomCore,
              child: Text("Clique"),
            ),
            Text(
              '$randomTexto',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),

      ),


    );
  }
}
