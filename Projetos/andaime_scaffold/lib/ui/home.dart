
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  void botaoFlutuante() {
    debugPrint("Botão flutuante");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Scaffold-Andaime"),
        backgroundColor: Colors.orange,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.accessible), onPressed: () => debugPrint("Teste ")),
          IconButton(icon: Icon(Icons.print), onPressed: () => debugPrint("Teste 2 ")),
          IconButton(icon: Icon(Icons.access_alarm), onPressed: () => debugPrint("Teste 3 ")),
        ],
      ),
      backgroundColor: Colors.grey,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Daniel",
            style: TextStyle(
              fontSize: 14.5,
              fontWeight: FontWeight.w700,
              color: Colors.orange
            ),)
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(icon: Icon(Icons.access_alarm),
          title: Text("Perfil")),
        BottomNavigationBarItem(icon: Icon(Icons.print),
            title: Text("Print")),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: botaoFlutuante,
          child: Icon(Icons.add),
          backgroundColor: Colors.orangeAccent,
      ),

    );
  }
}