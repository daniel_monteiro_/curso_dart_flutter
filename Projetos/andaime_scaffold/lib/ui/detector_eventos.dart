

import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetectorEventos extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detector"),
        backgroundColor: Colors.purpleAccent,
      ),
      body: Center(
        child: MeuBotao(),
      ),
    );
  }
}

class MeuBotao extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector( // Dector de eventos/gestos
      onTap: () {
        final snackBar = SnackBar(content: Text("Olá gestos/eventos"));
        Scaffold.of(context).showSnackBar(snackBar);
      },
      // Criar o tal botão
      child: Container(
        padding: EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(5.5)
        ),
        child: Text("O Meu Botão"),
      ),
    );
  }

}