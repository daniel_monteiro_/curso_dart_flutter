import 'package:flutter/material.dart';

class Magnata extends StatefulWidget {
  _MagnataState createState() => _MagnataState();
}

class _MagnataState extends State<Magnata> {
  int _contadorGrana = 0;

  void _mandaGrana() {

    setState(() {
      // Responsável pela atualização da tela com valores
      _contadorGrana += 100;

      if (_contadorGrana >= 6000) { // Muda cor aqui

      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Magnata"),
        backgroundColor: Colors.lightGreen,
      ),

      body: Container(
        child: Column(
          children: <Widget>[
            Center(
              child: Text("Fique rico!", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w400, fontSize: 29.9),),
            ),
            Expanded(child: Center(
              child: Text("\$ $_contadorGrana",
                style: TextStyle(fontSize: 45.6,
                    color: _contadorGrana >= 6000 ? Colors.blueAccent : Colors.amber, fontWeight: FontWeight.w700),
              ),
            )),

            Expanded(child: Center(child: FlatButton(onPressed: () => _mandaGrana(),
                color: Colors.lightGreen,
                textColor: Colors.white70,
                child: Text("Mais Grana!", style: TextStyle(fontSize: 25.8),)),))
            
          ],
        ),
      ),
    );
  }



}